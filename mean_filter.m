max_delay = 5;
data_matrix = zeros(length(EMG29),max_delay);
for i_delay = 1:max_delay
    data_matrix(:,i_delay) = [ EMG29(i_delay:end) zeros(1,i_delay-1) ]';
end
emg = median(data_matrix,2);
emg = [ 0 0 emg(1:end-4)' ];
figure(1);
plot(emg);
hold on;
plot(EMG29,'r');
hold off

filter = [-1 0 8 16 8 0 -1];
figure(2);
plot(EMG29,'r');hold on;
[ ~, ind ] = find(conv(emg,filter) > 2*std((conv(emg,filter))));
scatter(ind,EMG29(ind));hold off;
